import React, { ReactNode } from 'react'
import { useRouter } from 'next/router'

import useAppSettings from '@/application/appSettings/useAppSettings'
import { useHomeInfo } from '@/application/homeInfo'
import Button from '@/components/Button'
import Card from '@/components/Card'
import Col from '@/components/Col'
import Grid from '@/components/Grid'
import Icon from '@/components/Icon'
import Image from '@/components/Image'
import Link from '@/components/Link'
import NumberJelly from '@/components/NumberJelly'
import Row from '@/components/Row'
import Tooltip from '@/components/Tooltip'
import linkTo from '@/functions/dom/linkTo'
import useDevice from '@/hooks/useDevice'
import { useDocumentScrollDetector } from '@/hooks/useScrollDetector'
import useDocumentMetaTitle from '@/hooks/useDocumentMetaTitle'

function HomePageContainer({ children }: { children?: ReactNode }) {
  useDocumentScrollDetector()
  useDocumentMetaTitle('KERAMOS')
  const isMobile = useAppSettings((s) => s.isMobile)
  const isTabletv = useAppSettings((s) => s.isTabletv)
  return (

    <div
        className="flow-root overflow-x-hidden"
        style={{
          backgroundColor: '#25262B',
          backgroundRepeat: 'no-repeat',
          backgroundPositionX: 'center',
          backgroundImage:
            isMobile || isTabletv
              ? "url('/backgroundImages/home-bg-element-2.png')"
              : "url('/backgroundImages/home-bg-element-2.png')",
          backgroundSize:
            isMobile || isTabletv
            ? "920px"
            : "1480px",
          backgroundPositionY:
            isMobile || isTabletv
              ? "-60px"
              : "-180px"
        }}
      >
        {children}
    </div>
  )
}

function HomePageNavbar() {
  const isMobile = useAppSettings((s) => s.isMobile)
  const { push } = useRouter()
  return (
    <Row className="justify-between mobile:justify-center py-12 px-[min(160px,8vw)] tablet:px-[min(80px,4vw)]">
      <Image src="/logo/logo-with-text.svg" style={{height:'48px'}} 
      onClick={() => {
        linkTo('https://www.keramos.tech/')
      }}/>
      {!isMobile && (
        <Button
          className="frosted-glass-lightsmoke text-sm co-primary px-6 py-2"
          onClick={() => {
            linkTo('https://docs.google.com/viewer?url=https://raw.githubusercontent.com/keramostech/docs/main/Litepaper/Keramos_Litepaper_V1.pdf')
          }}
        >
          <div style={{float:'left', marginRight:'8px', marginTop:'1px'}}>LITEPAPER V1</div>
          <Icon iconSrc="/icons/rocket.svg" size="md" />
        </Button>
      )}
    </Row>
  )
}

function HomePageSection0() {
  const isMobile = useAppSettings((s) => s.isMobile)
  const isTabletv = useAppSettings((s) => s.isTabletv)
  const { push } = useRouter()
  const { tvl, totalvolume } = useHomeInfo()
  return (
    isMobile
    ?
    <section className="grid-child-center grid-cover-container mb-16 mt-2 relative">
      {/*<Image src="/backgroundImages/home-bg-element-1.webp" className="w-[744px] mobile:w-[394px]" />*/}
      <div className="grid-cover-content children-center">
        <div className="font-scplight text-36 co-primary mb-4 mt-14 mobile:mt-6 leading-40">
        “Hello, <br />&nbsp;DeFi World!”
        </div>
          <div className="font-scplight text-lg co-secondary mb-0" style={{fontStyle:'italic'}}>
          IN AN{' '}
          <span
            className="font-scplight text-transparent bg-clip-text"
            style={{
              background: 'radial-gradient(circle at top right,#819dd6,#ddc8f2)',
              backgroundClip: 'text',
              WebkitBackgroundClip: 'text'
            }}
          >
            AUTOMATED&nbsp; <br /> HIGH FREQUENCY&nbsp;
          </span>
          FASHION&nbsp;
          </div>
        {/* two button */}
        {/*
        <Row className="gap-8 mobile:gap-4 mb-16 mobile:mb-6 grid grid-cols-2-fr">
          <Button
            className="home-rainbow-button-bg text-[#25262B] text-sm mobile:text-xs px-5 mobile:px-4"
            onClick={() => {
              linkTo('https://dex.keramos.tech/#/market/9wFFyRfZBsuAha4YcuxcXLKwMxJR43S7fPfQLusDBzvT')
            }}
          >
            <Row className="items-center gap-2" style={{height:'2.6rem'}}>
              <div>LAUNCH APP</div>
              <Icon iconSrc="/icons/rocket.svg" size="md" />
            </Row>
          </Button>

          <Button
            className="frosted-glass-lightsmoke co-primary mobile:text-xs px-5 mobile:px-4 forsted-blur"
            onClick={() => {
              linkTo('https://keramos.gitbook.io/keramos-docs/')
            }}
          >
            <Row className="items-center gap-2">
              <div>READ DOCS</div>
              <Icon iconSrc="/icons/gitbook.svg" size="sm" />
            </Row>
          </Button>
        </Row>
          */}
        
        {/* two panels */}
        <Row className="gap-6 mobile:gap-3 mb-0 grid grid-rows-1-fr py-8 px-8">
          <Card className="frosted-glass-smoke forsted-blur-sm p-6 mobile:py-4 mobile:px-4 min-w-[250px] tablet:min-w-[250px]">
            <div className="text-xs co-secondary mb-1 mobile:text-xs">TOTAL VALUE LOCKED</div>
            {/* value */}
            <Row className="justify-center text-xl mobile:text-sm font-normal co-primary tracking-widest mobile:tracking-wider">
              <div className="mr-1">$</div>
              {tvl && (
                <NumberJelly
                  fractionLength={0}
                  eachLoopDuration={400}
                  totalDuration={8 * 60 * 1000}
                  currentValue={tvl}
                  initValue={tvl ? 0.999 * tvl : undefined}
                />
              )}
            </Row>
          </Card>

          <Card className="frosted-glass-smoke forsted-blur-sm p-6 mobile:py-4 mobile:px-4 min-w-[250px] tablet:min-w-[250px]">
            <div className="text-xs co-secondary mb-1 mobile:text-xs">TOTAL TRADING VOLUME</div>
            {/* value */}
            <Row className="justify-center text-xl mobile:text-sm font-normal co-primary tracking-widest mobile:tracking-wider">
              <div className="mr-1">$</div>
              {totalvolume && (
                <NumberJelly
                  fractionLength={0}
                  eachLoopDuration={200}
                  totalDuration={8 * 60 * 1000}
                  currentValue={totalvolume}
                  initValue={totalvolume ? 0.999 * totalvolume : undefined}
                />
              )}
            </Row>
          </Card>
        </Row>

        {/* built on solana */}
        <Image src="/logo/build-on-slogan.svg" style={{height:'32px'}} />

      </div>
    </section>
    :
    <section className="grid-child-center grid-cover-container mb-16 mt-6 relative">
      {/*<Image src="/backgroundImages/home-bg-element-1.webp" className="w-[744px] mobile:w-[394px]" />*/}
      <div className="grid-cover-content children-center">
        <div className="font-scplight text-40 co-primary mb-4 mt-14 mobile:mt-9 leading-[60px] mobile:leading-[32px]">
        “Hello, DeFi World!”
        </div>
          <div className="font-scplight text-28 co-secondary mb-8" style={{fontStyle:'italic'}}>
          IN AN{' '}
          <span
            className="font-scplight text-transparent bg-clip-text"
            style={{
              background: 'radial-gradient(circle at top right,#819dd6,#ddc8f2)',
              backgroundClip: 'text',
              WebkitBackgroundClip: 'text'
            }}
          >
            AUTOMATED HIGH FREQUENCY&nbsp;
          </span>
          FASHION&nbsp;
          </div>
        
        {/* two panels */}
        <Row className="gap-6 mobile:gap-3 mb-4 grid grid-cols-2-fr tabletv:grid-rows-1-fr py-8">
          <Card className="frosted-glass-smoke forsted-blur-sm p-6 mobile:py-3 mobile:px-6 mobile:min-w-[156px] min-w-[250px] tablet:min-w-[250px]">
            <div className="text-xs co-secondary mb-1 mobile:text-[8px]">TOTAL VALUE LOCKED</div>
            {/* value */}
            <Row className="justify-center text-xl mobile:text-xs font-normal co-primary tracking-widest mobile:tracking-wider">
              <div className="mr-1">$</div>
              {tvl && (
                <NumberJelly
                  fractionLength={0}
                  eachLoopDuration={400}
                  totalDuration={8 * 60 * 1000}
                  currentValue={tvl}
                  initValue={tvl ? 0.999 * tvl : undefined}
                />
              )}
            </Row>
          </Card>

          <Card className="frosted-glass-smoke forsted-blur-sm p-6 mobile:py-3 mobile:px-6 mobile:min-w-[156px] min-w-[250px] tablet:min-w-[250px]">
            <div className="text-xs co-secondary mb-1 mobile:text-[8px]">TOTAL TRADING VOLUME</div>
            {/* value */}
            <Row className="justify-center text-xl mobile:text-xs font-normal co-primary tracking-widest mobile:tracking-wider">
              <div className="mr-1">$</div>
              {totalvolume && (
                <NumberJelly
                  fractionLength={0}
                  eachLoopDuration={200}
                  totalDuration={8 * 60 * 1000}
                  currentValue={totalvolume}
                  initValue={totalvolume ? 0.999 * totalvolume : undefined}
                />
              )}
            </Row>
          </Card>
        </Row>

        {/* built on solana */}
        <Image src="/logo/build-on-slogan.svg" className="transform mobile:scale-75" style={{height:'36px'}} />

      </div>
    </section>
  )
}

function HomePageSection1() {
  const { push } = useRouter()
  return (
    <section
      className="grid-child-center overflow-hidden relative mx-auto tablet:mx-5 px-24 py-12 mt-6 tablet:p-8 max-w-[1320px] min-h-[506px] hidden"
      /*style={{
        background:
          "radial-gradient(at center top, transparent 20%, hsl(245, 60%, 16%, 0.2)), url('/backgroundImages/home-page-section1-light.webp'), #141416",
        boxShadow:
          '8px 8px 10px rgba(20, 16, 65, 0.05), -8px -8px 10px rgba(197, 191, 255, 0.05), inset 0 6px 20px rgba(197, 191, 255, 0.2), inset 0 -1px 25px rgba(197, 191, 255, 0.1)',
        backgroundSize: '100% 100%'
      }}*/
    >
      <div
        className="absolute inset-0 opacity-30 mb-16"
        /*style={{
          background: 'linear-gradient(245.22deg, #D47B9A 7.97%, #2b6aff 49.17%, #72D4B9 92.1%)',
          maskImage: "url('/backgroundImages/home-bg-element-2.webp')",
          WebkitMaskImage: "url('/backgroundImages/home-bg-element-2.webp')",
          maskSize: 'cover',
          WebkitMaskSize: 'cover'
        }}*/
      />
      <div>
        <div className="mb-6">
          {/*<div
            className="w-10 h-px my-2 mx-auto"
            style={{ background: 'radial-gradient(39.84% 47.5% at 96.82% 58.33%, #72D4B9 0%, #2b6aff 100%)' }}
      />*/}
          <div className="text-32 co-primary font-scplight mobile:text-28">LAUNCHPAD</div>
        </div>

        <Grid className="gap-8 grid-cols-4 tablet:grid-cols-4 tabletv:grid-cols-2 mobile:grid-cols-1">
          <Card className="flex-1 children-center frosted-glass-no-shadow forsted-blur-sm py-6 px-6">
            <div className="w-24 tabletv:w-32 mb-4" style={{marginTop:'-20px'}}>
              <img src='/backgroundImages/bot_dex.png' />
            </div>
            <div className="font-medium text-md co-primary mb-2 mobile:text-lg">DEX</div>
            <div className="font-light text-xs co-secondary mb-5 mobile:text-sm">Trade<br />Fast &#38; Low Fee</div>
            <Button
              className="frosted-glass-lightsmoke text-sm w-full mobile:text-lg"
              onClick={() => {
                linkTo('https://dex.keramos.tech/#/market/9wFFyRfZBsuAha4YcuxcXLKwMxJR43S7fPfQLusDBzvT')
              }}
            >
              TRADE
            </Button>
          </Card>

          <Card className="flex-1 children-center frosted-glass-no-shadow forsted-blur-sm py-6 px-6">
            <div className="w-24 tabletv:w-32 mb-4" style={{marginTop:'-20px'}}>
              <img src='/backgroundImages/bot_amm.png' />
            </div>
            <div className="font-medium text-md co-primary mb-2 mobile:text-lg">AMM</div>
            <div className="font-light text-xs co-secondary mb-5 mobile:text-sm">Access to order flow<br />and liquidity</div>
            <Button
              className="frosted-glass-lightsmoke text-sm w-full mobile:text-lg"
              onClick={() => {
                push('/swap')
              }}
            >
              SWAP
            </Button>
          </Card>
          
          <Card className="flex-1 children-center frosted-glass-no-shadow forsted-blur-sm py-6 px-6">
            <div className="w-24 tabletv:w-32 mb-4" style={{marginTop:'-20px'}}>
              <img src='/backgroundImages/bot_pool.png' />
            </div>
            <div className="font-medium text-md co-primary mb-2 mobile:text-lg">POOLS ACROSS PLATFORM</div>
            <div className="font-light text-xs co-secondary mb-5 mobile:text-sm">One Platform One Pool<br />Many more opportunities</div>
            <Button
              className="frosted-glass-lightsmoke text-sm w-full mobile:text-lg"
              onClick={() => {
                push('/pools')
              }}
            >
              VIEW POOLS
            </Button>
          </Card>

          <Card className="flex-1 children-center frosted-glass-no-shadow forsted-blur-sm py-6 px-6">
            <div className="w-24 tabletv:w-32 mb-4" style={{marginTop:'-20px'}}>
              <img src='/backgroundImages/bot_farm.png' />
            </div>
            <div className="font-medium text-md co-primary mb-2 mobile:text-lg">YIELD FARM</div>
            <div className="font-light text-xs co-secondary mb-5 mobile:text-sm">Ready-to-deploy<br />Multiple Yield Strategies</div>
            <Button
              className="frosted-glass-lightsmoke text-sm w-full mobile:text-lg"
              onClick={() => {
                push('/farms')
              }}
            >
              FARM
            </Button>
          </Card>

        </Grid>
      </div>
    </section>
  )
}

function HomePageSection3() {
  return (
    <section className="children-center mb-24 py-12 mt-12">
      <div className="mb-12">
        <div className="text-32 font-scplight">PARTNERS</div>
        {/*<div
          className="w-10 h-px my-2 mx-auto rounded-full"
          style={{ background: 'radial-gradient(39.84% 47.5% at 96.82% 58.33%, #72D4B9 0%, #2b6aff 100%)' }}
  />*/}
      </div>
      <Row className="w-full justify-center px-56 mb-12 mobile:px-0 mobile:grid gap-24 mobile:gap-12 h-22rem">
        <Image src="/logo/solana-text-logo.svg" style={{height:'32px'}} />
        <Image src="/logo/serum-text-logo.svg" style={{height:'32px'}} />
        <Image src="/logo/raydium-text-logo.svg" style={{height:'32px'}} />
      </Row>
      <div style={{height:'4rem'}} />
    </section>
  )
}

export default function HomePage() {
  return (
    <HomePageContainer>
      <HomePageNavbar />
      <HomePageSection0 />
      <HomePageSection1 />
      {/*<HomePageSection2 />*/}
      <HomePageSection3 />
      {/*<HomePageFooter />*/}
    </HomePageContainer>
  )
}
