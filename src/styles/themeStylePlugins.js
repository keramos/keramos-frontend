const plugin = require('tailwindcss/plugin')

exports.cyberpunkLightBorders = plugin(({ addUtilities }) => {
  const cyberpunkLightBorders = {
    '.cyberpunk-border': {
      borderColor: 'transparent',
      position: 'relative',
      '&::after': {
        content: "''",
        position: 'absolute',
        top: '0',
        right: '0',
        bottom: '0',
        left: '0',
        pointerEvents: 'none',
        overflow: 'hidden',
        background: 'linear-gradient(90deg, #d3eaed 0%, #e2cdf7 30%, #86a2db 100%)',
        boxShadow: '0 3px 6px -4px rgb(206 229 232 / 48%), 0 6px 16px 0 rgb(221 200 242 / 32%), 0 9px 28px 8px rgb(164 170 245 / 20%)',
        borderRadius: '0'
        /*maskImage: `url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' fill='none' stroke='black' stroke-width='1.5'/></svg>")`*/
      }
    }
  }
  addUtilities(cyberpunkLightBorders, ['focus-within', 'hover', 'active'])

  const roundedValueMap = {
    '-sm': '0',
    '': '0',
    '-md': '0',
    '-lg': '0',
    '-xl': '0',
    '-2xl': '0',
    '-3xl': '0'
  }
  const resultMap = Object.fromEntries(
    Object.entries(roundedValueMap).map(([roundClass, roundedValue]) => [
      `.cyberpunk-border-rounded${roundClass}`,
      {
        '&::after': {
          /*maskImage: `url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'><rect width='100%' height='100%' rx='${roundedValue}' fill='none' stroke='black' stroke-width='1.5'/></svg>")`*/
        }
      }
    ])
  )
  addUtilities(resultMap, ['focus-within', 'hover', 'active'])
})

exports.cyberpunkBgLight = plugin(({ addUtilities }) => {
  addUtilities(
    {
      '.cyberpunk-bg-light': {
        position: 'relative',
        '&::before': {
          content: "''",
          position: 'absolute',
          top: '-15px',
          right: '-35px',
          bottom: '-15px',
          left: '-35px',
          zIndex: '-1', // !!! is't parent node must have a new stacking context (like css isolation)
          pointerEvents: 'none',
          background:
          'radial-gradient(circle,#515A8C 10%, #2D2F3D 50%, #222327 75%)',
          filter: 'blur(90px)',
          opacity: '.45'
        }
      },
      '.cyberpunk-bg-light-simi': {
        position: 'relative',
        '&::before': {
          content: "''",
          position: 'absolute',
          top: '-15px',
          right: '-35px',
          bottom: '-15px',
          left: '-35px',
          zIndex: '-1',
          pointerEvents: 'none',
          background:
          'radial-gradient(circle,#515A8C 10%, #2D2F3D 50%, #222327 75%)',
          filter: 'blur(90px)',
          opacity: '.45'
        }
      }
    },
    ['hover', 'active']
  )
})

exports.glassStyle = plugin(({ addUtilities }) => {
  addUtilities({
    '.frosted-glass-smoke , .frosted-glass-lightsmoke , .frosted-glass-teal , .frosted-glass-skygray , .frosted-glass':
      {
        '--text-color': 'hsl(238, 100%, 95%)',
        '--border-color': 'hsl(0, 0%, 100%)',
        '--bg-board-color': 'hsl(0, 0%, 100%, 0.12)',
        '--bg-board-color-2': 'hsl(0, 0%, 100%, 0)',

        position: 'relative',
        backdropFilter: 'blur(calc(var(--blur-size) * (-1 * var(--is-scrolling, 0) + 1)))',
        color: 'var(--text-color)',
        background:
          '#2A2B32',
        isolation: 'isolate',
        '&::before': {
          content: "''",
          position: 'absolute',
          inset: 0,
          zIndex: '-1',
          opacity: '1',
          background: 'transparent',
          borderRadius: '0',
          border: '1px solid transparent',
          /*boxShadow: 'inset 0 0 0 var(--border-line-width, 1.5px) var(--border-color)',*/
          /*maskImage: `radial-gradient(at -31% -58%, hsl(0, 0%, 0%, 0.5) 34%, transparent 60%),
          linear-gradient(to left, hsl(0, 0%, 0%, 0.2) 0%, transparent 13%),
          linear-gradient(hsl(0deg 0% 0% / 5%), hsl(0deg 0% 0% / 5%))`,*/
          /*border: '1px solid',
          borderImage: 'linear-gradient(90deg, #d3eaed 0%, #e2cdf7 30%, #86a2db 100%) 1',*/
          boxShadow: 'rgb(0 0 0 / 48%) 0px 3px 6px -4px, rgb(0 0 0 / 32%) 0px 6px 16px 0px, rgb(0 0 0 / 20%) 0px 9px 28px 8px'
        },
        '&::hover': {
          backgroundImage: 'linear-gradient(90deg, #6a7578 0%, #70687d 30%, #4c5872 100%)',
          border: '1px solid',
          borderImage: 'linear-gradient(90deg, #d3eaed 0%, #e2cdf7 30%, #86a2db 100%) 1'
        }
      },
    '.frosted-glass-teal': {
      '--text-color': 'hsl(183, 67%, 54%)',
      '--border-color': 'hsl(165, 87%, 65%)',
      '--bg-board-color': 'hsl(183, 67%, 54%, 0.2)',
      '--bg-board-color-2': 'hsl(183, 67%, 54%, 0)'
    },
    '.frosted-glass-skygray': {
      '--text-color': '#A4AAF5',
      '--border-color': '#A4AAF5',
      '--bg-board-color': 'rgba(171, 196, 255, 0.2)',
      '--bg-board-color-2': 'rgba(171, 196, 255, 0)'
    },
    '.frosted-glass-lightsmoke': {
      '--bg-board-color': 'hsl(233, 9%, 18%, 0.08)',
      '--bg-board-color-2': 'hsl(233, 9%, 18%, 0)',
      '--text-color': 'hsl(238, 100%, 95%)'
    },
    '.frosted-glass-smoke': {
      '--border-color': 'hsl(0, 0%, 100%)',
      '--bg-board-color': 'hsl(0, 0%, 100%, 0.12)',
      '--text-color': 'hsl(0, 0%, 100%)'
    },
    '.forsted-blur-lg': {
      '--blur-size': '6px'
    },
    '.forsted-blur': {
      '--blur-size': '3px'
    },
    '.forsted-blur-sm': {
      '--blur-size': '2px'
    },
    '.frosted-blur-none': {
      '--blur-size': '0'
    }
  })

  addUtilities({
    '.home-rainbow-button-bg': {
      borderRadius: '0',
      background: 'linear-gradient(90deg, #d3eaed 0%, #e2cdf7 30%, #86a2db 100%)',
      backgroundPosition: '30% 50%',
      backgroundSize: '150% 150%',
      transition: '500ms',
      '&:hover': {
        backgroundImage: 'linear-gradient(90deg, #6a7578 0%, #70687d 30%, #4c5872 100%)',
        border: '1px solid',
        borderImage: 'linear-gradient(90deg, #d3eaed 0%, #e2cdf7 30%, #86a2db 100%) 1'
      }
    }
  })
})

// TODO
// exports.coinRotateLoop = plugin(({ addUtilities }) => {
//   addUtilities({
//     '.swap-coin': {
//       position: 'relative',
//       animation: 'rotate-y-infinite 2s infinite',
//       animationDelay: 'var(--delay, 0)',
//       transformStyle: 'preserve-3d',
//       '.line-group': {
//         position: 'absolute',
//         top: '0',
//         right: '0',
//         bottom: '0',
//         left: '0',
//         transformStyle: 'inherit',
//         '.line-out': {
//           top: '50%',
//           position: 'absolute',
//           left: '50%',
//           width: '50%',
//           transformOrigin: 'left',
//           transformStyle: 'inherit'
//         },
//         '.line-inner': {
//           position: 'absolute',
//           left: '100%',
//           backgroundColor: 'var(--ground-color-dark-solid)',
//           transform: 'translateY(-50%) translateX(-64%) rotateY(90deg)',
//           width: '6px',
//           height: '13px',
//           border: '1px solid rgba(255, 255, 255, 0.5)',
//           borderLeft: 'none',
//           borderRight: 'none'
//         }

//       }
//     }
//   })
// })
