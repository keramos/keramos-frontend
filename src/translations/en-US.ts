import type { AppTranslation } from './interface'

// TODO: complete this
const enUS: AppTranslation = {
  'global|route swap': 'SWAP',
  'global|connect(wallet)': 'CONNECT',
  'global|select language': 'select language',
  'global|change rpc': 'change rpc',
  'swap|To (estimated)': 'TO (estimated)',
  'swap|To': 'TO',
  'swap|From': 'FROM',
  'swap|Select a Token': 'Select a Token',
  'swap|swap button text': 'SWAP',
  'swap|liquidity button text': 'SUPPLY'
}
export default enUS
