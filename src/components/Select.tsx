import useAppSettings from '@/application/appSettings/useAppSettings'
import { isString } from '@/functions/judgers/dateType'
import React, { useMemo } from 'react'
import { twMerge } from 'tailwind-merge'
import Col from './Col'
import Collapse from './Collapse'
import Icon from './Icon'
import Row from './Row'

/**
 * styled component
 */
export default function Select<T extends string>({
  className,
  candidateValues,
  defaultValue,
  prefix,
  onChange
}: {
  className?: string
  candidateValues: (T | { label: string; value: T })[]
  defaultValue?: string
  prefix?: string
  onChange?: (value: T | undefined /* emptify */) => void
}) {
  const [currentValue, setCurrentValue] = React.useState(defaultValue)
  const isMobile = useAppSettings((s) => s.isMobile)

  const parsedCandidates = useMemo(
    () => candidateValues.map((i) => (isString(i) ? { label: i, value: i } : i)),
    [candidateValues]
  )

  const currentLable = useMemo(
    () => parsedCandidates.find(({ value }) => value === currentValue)?.label,
    [currentValue, parsedCandidates]
  )

  const [isOpen, setIsOpen] = React.useState(false)

  const FaceCotent = ({ open = false }) => (
    <Row className="items-center w-full">
      <div className="mobile:text-xs text-xs font-medium text-[#80859C] mr-1 whitespace-nowrap">
        {prefix}
      </div>
      <div className="grow mobile:text-xs text-xs font-medium text-[#A4AAF5] whitespace-nowrap">
        {currentLable}
      </div>
      <Icon
        size={isMobile ? 'xs' : 'sm'}
        className="justify-self-end mr-1.5 text-[#A4AAF5] ml-2"
        heroIconName={`${open ? 'chevron-up' : 'chevron-down'}`}
      />
    </Row>
  )
  return (
    <div className={twMerge('relative', className)}>
      <div
        className={`py-2 px-4 mobile:px-3 ring-inset ring-1.5 ring-[rgba(196,214,255,0.1)] bg-[#1F2024] h-full invisible`}
      >
        <FaceCotent />
      </div>
      <Collapse
        className={`absolute z-10 top-0 left-0 ring-inset ring-1.5 ring-[rgba(196,214,255,0.1)] bg-[#1F2024] w-full`}
        style={{
          background: isOpen
            ? '#1F2024'
            : ''
        }}
        onClose={() => setIsOpen(false)}
        onOpen={() => setIsOpen(true)}
        closeByOutsideClick
      >
        <Collapse.Face>
          {(open) => (
            <div className="py-2 px-4 mobile:px-3 ">
              <FaceCotent open={open} />
            </div>
          )}
        </Collapse.Face>
        <Collapse.Body>
          {(open, controller) => (
            <Col className="border-t-1.5 border-[rgba(164,170,245,.1)] px-3 py-1">
              {candidateValues.map((candidate) => {
                const { label, value } =
                  typeof candidate === 'string' ? { label: candidate, value: candidate } : candidate
                return (
                  <Row
                    key={value}
                    className={`mobile:text-xs text-xs font-medium py-1.5 hover:text-[#A4AAFF] text-[#80859C] cursor-pointer ${
                      value === currentValue ? 'text-[#A4AAFF]' : ''
                    } items-center`}
                    onClick={() => {
                      const parsedValue = value === currentValue ? undefined : value
                      setCurrentValue(parsedValue)
                      onChange?.(parsedValue)
                      controller.close()
                    }}
                  >
                    {label}
                    {value === currentValue && <Icon size="sm" heroIconName="check" className="ml-2" />}
                  </Row>
                )
              })}
            </Col>
          )}
        </Collapse.Body>
      </Collapse>
    </div>
  )
}
