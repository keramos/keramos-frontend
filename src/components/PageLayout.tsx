import React, { CSSProperties, ReactNode, useEffect, useMemo, useRef, useState } from 'react'
import { useRouter } from 'next/router'

import { ZERO } from '@raydium-io/raydium-sdk'

import { twMerge } from 'tailwind-merge'

import useAppSettings from '@/application/appSettings/useAppSettings'
import useConnection from '@/application/connection/useConnection'
import useNotification from '@/application/notification/useNotification'
import useWallet from '@/application/wallet/useWallet'
import jFetch from '@/functions/dom/jFetch'
import linkTo from '@/functions/dom/linkTo'
import { eq } from '@/functions/numberish/compare'
import { div, mul } from '@/functions/numberish/operations'
import { toString } from '@/functions/numberish/toString'
import useAsyncEffect from '@/hooks/useAsyncEffect'
import useAsyncMemo from '@/hooks/useAsyncMemo'
import useDocumentMetaTitle from '@/hooks/useDocumentMetaTitle'
import { LinkAddress } from '@/types/constants'

import { Badge } from './Badge'
import Button from './Button'
import Col from './Col'
import Drawer from './Drawer'
import { FadeIn } from './FadeIn'
import Grid from './Grid'
import Icon, { AppHeroIconName } from './Icon'
import Image from './Image'
import Input from './Input'
import Link from './Link'
import MessageBoardWidget from './navWidgets/MessageBoardWidget'
import WalletWidget from './navWidgets/WalletWidget'
import PageLayoutPopoverDrawer from './PageLayoutPopoverDrawer'
import Row from './Row'
import Tooltip from './Tooltip'
import LoadingCircle from './LoadingCircle'
import { setCssVarible } from '@/functions/dom/cssVariable'
import { inClient } from '@/functions/judgers/isSSR'
import { useAppVersion } from '@/application/appVersion/useAppVersion'
import { refreshWindow } from '@/application/appVersion/forceWindowRefresh'
import Card from './Card'
import Dialog from './Dialog'

/**
 * for easier to code and read
 *
 * TEMP: add haveData to fix scrolling bug
 */
export default function PageLayout(props: {
  /** only mobile  */
  mobileBarTitle?: string
  metaTitle?: string
  children?: ReactNode
  className?: string
  contentClassName?: string
  topbarClassName?: string
  sideMenuClassName?: string

  contentIsFixedLength?: boolean // it will cause content bottom padding shorter than usual

  showWalletWidget?: boolean
  showRpcWidget?: boolean
  showLanguageWidget?: boolean
}) {
  useDocumentMetaTitle(props.metaTitle)
  const isMobile = useAppSettings((s) => s.isMobile)
  const [isSideMenuOpen, setIsSideMenuOpen] = useState(false)
  return (
    <div
      style={{
        padding:
          'env(safe-area-inset-top, 0px) env(safe-area-inset-right, 0px) env(safe-area-inset-bottom, 0px) env(safe-area-inset-left, 0px)',
        position: 'relative',
        display: 'grid',
        gridTemplate: isMobile
          ? `
          "d" auto
          "a" auto
          "c" 1fr / 1fr`
          : `
          "d d d" auto
          "a a a" auto
          "b c c" 1fr
          "b c c" 1fr / auto 1fr 1fr`,
        overflow: 'hidden', // establish a BFC
        willChange: 'opacity'
      }}
      className={`w-screen mobile:w-full h-screen mobile:h-full`}
    >
      <RPCPerformanceBanner className="grid-area-d" />
      {isMobile ? (
        <>
          <Navbar barTitle={props.mobileBarTitle} className="grid-area-a" onOpenMenu={() => setIsSideMenuOpen(true)} />
          <Drawer
            open={isSideMenuOpen}
            onCloseTransitionEnd={() => setIsSideMenuOpen(false)}
            onOpen={() => setIsSideMenuOpen(true)}
          >
            {({ close }) => <SideMenu className="flex-container h-screen" onClickCloseBtn={close} />}
          </Drawer>
        </>
      ) : (
        <>
          <Navbar className="grid-area-a" />
          <SideMenu className="flex-container grid-area-b" />
        </>
      )}
      <main
        // always occupy scrollbar space
        className={twMerge(
          `PageLayoutContent relative isolate flex-container grid-area-c bg-aura p-12 ${
            props.contentIsFixedLength ? 'pb-4' : ''
          } mobile:py-2.5 mobile:px-3`,
          props.contentClassName
        )}
        style={{
          contentVisibility: 'auto',
          overflowX: 'hidden',
          overflowY: 'scroll'
        }}
      >
        {/* do not check ata currently
        <MigrateBubble /> */}
        <VersionTooOldDialog />
        {props.children}
      </main>
    </div>
  )
}
function RPCPerformanceBanner({ className }: { className?: string }) {
  const { connection, currentEndPoint } = useConnection()
  const [isLowRpcPerformance, setIsLowRpcPerformance] = useState(false)

  const MAX_TPS = 1500 // force settings

  useAsyncEffect(async () => {
    if (isLowRpcPerformance) return // no need calc again
    if (!currentEndPoint?.url) return
    const result = await jFetch<{
      result: {
        numSlots: number
        numTransactions: number
        samplePeriodSecs: number
        slot: number
      }[]
    }>(currentEndPoint?.url, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: 'getRecentPerformanceSamples',
        jsonrpc: '2.0',
        method: 'getRecentPerformanceSamples',
        params: [4]
      })
    })
    if (!result) return
    const blocks = result.result
    const perSecond = blocks.map(({ numTransactions }) => numTransactions / 60)
    const tps = perSecond.reduce((a, b) => a + b, 0) / perSecond.length

    setIsLowRpcPerformance(tps < MAX_TPS)
  }, [connection])

  return (
    <div className={className}>
      <FadeIn>
        {isLowRpcPerformance && (
          <div className="bg-[#111111] text-center text-[#72D4B9] text-xs mobile:text-xs px-4 py-1">
            The Solana network is experiencing congestion or reduced performance. Transactions may fail to send or
            confirm.
          </div>
        )}
      </FadeIn>
    </div>
  )
}
function VersionTooOldDialog() {
  const versionRefreshData = useAppVersion((s) => s.versionFresh)
  return (
    <Dialog open={versionRefreshData === 'too-old'} canClosedByMask={false}>
      {({ close }) => (
        <Card
          className={twMerge(`p-8 w-[min(480px,95vw)] mx-8 border-1.5 border-[rgba(164,170,245,0.2)]`)}
          size="lg"
          style={{
            background:
              '#25262B',
            boxShadow: '0px 8px 48px rgba(171, 196, 255, 0.12)'
          }}
        >
          <Col className="items-center">
            <div className="font-semibold text-xl text-[#D4B798] mb-3 text-center">New version available</div>
            <div className="text-center mt-2  mb-6 text-[#A4AAF5]">Refresh the page to update and use the app.</div>

            <div className="self-stretch">
              <Col>
                <Button
                  className={`text-[#A4AAF5]  frosted-glass-lightsmoke`}
                  onClick={() => refreshWindow({ noCache: true })}
                >
                  Refresh
                </Button>
                <Button className="text-[#A4AAF5]" type="text" onClick={close}>
                  Update later
                </Button>
              </Col>
            </div>
          </Col>
        </Card>
      )}
    </Dialog>
  )
}

function MigrateBubble() {
  const noNeedAtaTokenMints = useAsyncMemo(async () => (await jFetch('/no-ata-check-token-mints.json')) as string[])
  const rawTokenAccounts = useWallet((s) => s.allTokenAccounts)
  const connected = useWallet((s) => s.connected)

  const needMigrate = useMemo(() => {
    const isInWhiteList = (mint: string) => (noNeedAtaTokenMints ?? []).includes(String(mint))
    return rawTokenAccounts.some(
      (tokenAccount) =>
        !tokenAccount.isNative &&
        !tokenAccount.isAssociated &&
        !isInWhiteList(String(tokenAccount.mint)) &&
        tokenAccount.amount.gt(ZERO)
    )
  }, [rawTokenAccounts, noNeedAtaTokenMints])

  useEffect(() => {
    if (needMigrate) {
      useNotification.getState().logWarning(
        'You have old tokenAccount',
        <div>
          please migrate to new ATA account
        </div>
      )
    }
  }, [needMigrate])

  if (!connected) return null
  if (!noNeedAtaTokenMints) return null // haven't load data
  if (!needMigrate) return null

  return (
    <Grid className="pc:grid-cols-[auto,1fr,auto] mobile:grid-cols-[auto,1fr] gap-3 justify-between items-center py-6 px-8 mb-10 ring-1.5 ring-inset ring-[#D47B9A] bg-[#141416]">
      <Icon className="text-[#D47B9A] self-start mobile:row-span-2" heroIconName="exclamation-circle" />
      <Col className="gap-2">
        <div className="co-primary text-base font-medium">You have old tokenAccount </div>
        <div className="text-[#C4D6FF] text-sm font-medium">
          Your wallet have old normal tokenAccount. Please migrate to ATA tokenAccount for safer transaction.
        </div>
      </Col>
      {/*
      <Row className="gap-8">
        <Button
          className=" mobile:flex-grow frosted-glass-lightsmoke w-40"
          onClick={() => linkTo('https://v1.raydium.io/migrate/')}
        >
          Migrate
        </Button>
        <Button
          className=" mobile:flex-grow ring-1.5 ring-inset ring-current opacity-50 text-[#A4AAF5] text-sm font-medium"
          type="text"
          onClick={() => linkTo('https://raydium.gitbook.io/raydium/updates/associated-token-account-migration')}
        >
          Learn More
        </Button>
      </Row>
      */}
    </Grid>
  )
}

function Navbar({
  barTitle,
  className,
  style,
  onOpenMenu
}: {
  className?: string
  barTitle?: string
  style?: CSSProperties
  // TODO: move it into useAppSetting()
  onOpenMenu?: () => void
}) {
  const isMobile = useAppSettings((s) => s.isMobile)
  const pcNavContent = (
    <Row className="justify-between items-center">
      <Link href="https://www.keramos.tech/">
        <Image className="cursor-pointer" style={{height:'40px', marginLeft:'1px'}} src="/logo/logo-with-text.svg" />
      </Link>

      <Row className="gap-8 items-center">
        <MessageBoardWidget />
        <WalletWidget />
      </Row>
    </Row>
  )
  const mobileNavContent = (
    <Grid className="grid-cols-3 items-center">
      <div className="frosted-glass-lightsmoke p-2 clickable justify-self-start" onClick={onOpenMenu}>
        <Icon className="w-4 h-4" iconClassName="w-4 h-4" iconSrc="/icons/msic-menu.svg" />
      </div>

      {barTitle ? (
        <div onClick={onOpenMenu} className="text-md font-medium place-self-center co-primary -mb-1">
          {barTitle}
        </div>
      ) : (
        <Link className="place-self-center" href="/">
          <Image className="cursor-pointer" src="/logo/logo-only-icon.svg" />
        </Link>
      )}

      <Row className="gap-4 items-center justify-self-end">
        <MessageBoardWidget />
        <WalletWidget />
      </Row>
    </Grid>
  )
  return (
    <nav
      className={twMerge('select-none co-primary px-8 py-6 mobile:px-5 mobile:py-3 transition-all', className)}
      style={{}}
    >
      {isMobile ? mobileNavContent : pcNavContent}
    </nav>
  )
}

function SideMenu({ className, onClickCloseBtn }: { className?: string; onClickCloseBtn?(): void }) {
  const { pathname } = useRouter()
  const isMobile = useAppSettings((s) => s.isMobile)
  const sideMenuRef = useRef<HTMLDivElement>(null)
  const lastestVersion = useAppVersion((s) => s.lastest)
  const currentVersion = useAppVersion((s) => s.currentVersion)

  useEffect(() => {
    if (!inClient) return
    setCssVarible(
      globalThis.document.documentElement,
      '--side-menu-width',
      sideMenuRef.current ? Math.min(sideMenuRef.current.clientWidth, sideMenuRef.current.clientHeight) : 0
    )
  }, [sideMenuRef])

  return (
    <>
      <Col
        domRef={sideMenuRef}
        className={twMerge(
          `h-full overflow-y-auto w-56a mobile:w-48`,
          className
        )}
        style={{
          background: isMobile
            ? 'rgba(37, 38, 43, 0.5), #25262B'
            : undefined,
          boxShadow: isMobile ? '8px 0px 48px rgba(0, 0, 0, 0.5)' : undefined
        }}
      >
        {isMobile && (
          <Row className="items-center justify-between p-6 mobile:p-4 mobile:pl-8">
            <Link href="https://www.keramos.tech/">
              <Image src="/logo/logo-with-text.svg" className="mobile:scale-75" />
            </Link>
            <Icon
              size={isMobile ? 'sm' : 'md'}
              heroIconName="x"
              className="text-[#80859c] clickable clickable-mask-offset-2"
              onClick={onClickCloseBtn}
            />
          </Row>
        )}
        <Col className="grid grid-rows-[1.1fr,1fr,auto] flex-1 overflow-hidden">
          <div className="shrink min-h-[120px] py-2 space-y-1 mobile:py-0 px-4 mobile:px-0 mr-2 mobile:ml-2 mb-2 mobile:h-[320px]">
            <LinkItem icon="/icons/rocket.svg" href="/" >
              LAUNCHPAD
            </LinkItem>
            <LinkItem icon="/icons/entry-icon-trade.svg" href="https://dex.keramos.tech/">
              TRADE
            </LinkItem>
            <LinkItem icon="/icons/entry-icon-swap.svg" href="/swap" isCurrentRoutePath={pathname.includes('swap')}>
              SWAP
            </LinkItem>
            <LinkItem icon="/icons/entry-icon-pools.svg" href="/pools" isCurrentRoutePath={pathname.includes('pools')}>
              POOLS
            </LinkItem>
            <LinkItem icon="/icons/entry-icon-farms.svg" href="/farms" isCurrentRoutePath={pathname.includes('farms')}>
              FARMS
            </LinkItem>
            <LinkItem
              icon="/icons/entry-icon-liquidity.svg"
              href="/liquidity/add"
              isCurrentRoutePath={pathname.includes('liquidity')}
            >
              ADD LIQUIDITY
            </LinkItem>
          </div>

          <Col className="mobile:h-[200px] overflow-scroll no-native-scrollbar px-4 mobile:px-0">
            <div className="mx-8 border-b border-[#1F2023] my-2 mobile:my-1"></div>
            <div className="flex-1 overflow-auto no-native-scrollbar mt-2 co-grey">
              {/*<RpcConnectionPanelSidebarWidget />*/}
              <SettingSidebarWidget />
              {/*<CommunityPanelSidebarWidget />*/}

              <OptionItem noArrow href="https://keramos.gitbook.io/keramos-docs/" iconSrc="/icons/msic-docs.svg">
                READ DOCS
              </OptionItem>

              <OptionItem noArrow href="https://keramos.gitbook.io/keramos-docs/faq/overview" iconSrc="/icons/msic-docs.svg">
                FAQ
              </OptionItem>

              <OptionItem noArrow href="https://docs.google.com/viewer?url=https://raw.githubusercontent.com/keramostech/docs/main/Litepaper/Keramos_Litepaper_V1.pdf" iconSrc="/icons/msic-litepaper.svg">
                LITEPAPER V1
              </OptionItem>

              {/*<OptionItem noArrow href="https://forms.gle/DvUS4YknduBgu2D7A" iconSrc="/icons/misc-feedback.svg">
                Feedback
              </OptionItem>*/}
            </div>
          </Col>

          <div className="px-6 py-4 text-xs m-2 opacity-20 hover:opacity-100 transition font-medium text-[#abc4ff] whitespace-nowrap">
            <div>V.0.3 Beta</div>
          </div>
        </Col>
      </Col>
    </>
  )
}

function LinkItem({
  children,
  href,
  icon,
  isCurrentRoutePath
}: {
  children?: ReactNode
  href?: string
  icon?: string
  isCurrentRoutePath?: boolean
}) {
  const isInnerLink = href?.startsWith('/')
  const isExternalLink = !isInnerLink
  const isMobile = useAppSettings((s) => s.isMobile)
  return (
    <Link
      href={href}
      noTextStyle
      className={`group block menutab py-2 mobile:py-2 px-4 mobile:px-1 hover:bg-[rgba(42,43,50,0.8)] ${
        isCurrentRoutePath ? 'bg-[rgba(42,43,50,1)] menutabactive' : ''
      }`}
    >
      <Row className="items-center">
        <div className="grid bg-gradient-to-br from-[rgba(57,208,216,0)] to-[rgba(57,208,216,0)] p-1.5 mr-3">
          <Icon size={isMobile ? 'xs' : 'sm'} iconSrc={icon} />
        </div>
        <Row
          className={`grow items-center justify-between text-[#80859C] hover:text-[#A4AAF5] ${
            isCurrentRoutePath ? 'text-[#E3E4FF]' : ''
          } text-sm mobile:text-xs font-medium`}
        >
          <div>{children}</div>
          {isExternalLink && (
            <Icon inline className="opacity-80" size={isMobile ? 'xs' : 'sm'} heroIconName="external-link" />
          )}
        </Row>
      </Row>
    </Link>
  )
}

function OptionItem({
  noArrow,
  children,
  iconSrc,
  heroIconName,
  href,
  onClick
}: {
  noArrow?: boolean
  children: ReactNode
  iconSrc?: string
  heroIconName?: AppHeroIconName
  href?: LinkAddress
  onClick?(): void
}) {
  const isMobile = useAppSettings((s) => s.isMobile)
  return (
    <Link
      href={href}
      noTextStyle
      className="block py-2.5 menutab mobile:py-2.5 px-8 pl-6 mobile:px-5 hover:bg-[rgba(42,43,50,0.8)] active:bg-[rgba(42,43,50,1)] cursor-pointer group"
    >
      <Row className="items-center w-full mobile:justify-center" onClick={onClick}>
        <Icon
          className="mr-3 text-[#80859C] hover:text-[#A4AAF5]"
          size={isMobile ? 'xs' : 'sm'}
          iconSrc={iconSrc}
          heroIconName={heroIconName}
        />
        <span
          className={`text-[#80859C] hover:text-[#A4AAF5] text-sm mobile:text-xs font-medium flex-grow ${
            href ? 'group-hover:text-[#A4AAF5]' : ''
          }`}
        >
          {children}
        </span>
        {!noArrow && <Icon size={isMobile ? 'xs' : 'sm'} heroIconName="chevron-right" iconClassName="text-[#80859C]" />}
      </Row>
    </Link>
  )
}

function SettingSidebarWidget() {
  return (
    <PageLayoutPopoverDrawer renderPopoverContent={<SettingPopover />}>
      <OptionItem iconSrc="/icons/msic-settings.svg">Settings</OptionItem>
    </PageLayoutPopoverDrawer>
  )
}

function SettingPopover() {
  const slippageTolerance = useAppSettings((s) => s.slippageTolerance)
  const slippageToleranceState = useAppSettings((s) => s.slippageToleranceState)
  return (
    <div className="py-5 px-6">
      <Row className="items-center mb-3 mobile:mb-6 gap-2">
        <div className="text-[rgba(102,108,128,1)] text-xs mobile:text-sm">SLIPPAGE TOLERANCE</div>
        <Tooltip placement="bottom-right">
          <Icon size="sm" heroIconName="question-mark-circle" className="cursor-help text-[rgba(164,170,245,0.5)]" />
          <Tooltip.Panel>The maximum difference between your estimated price and execution price</Tooltip.Panel>
        </Tooltip>
      </Row>
      <Row className="gap-3 justify-between">
        <div
          className={`py-1 px-3 bg-[#1F2024] text-[#E3E4FF] font-medium text-sm ${
            eq(slippageTolerance, 0.001) ? 'ring-1 ring-inset ring-[#72D4B9]' : ''
          } cursor-pointer`}
          onClick={() => {
            useAppSettings.setState({ slippageTolerance: '0.001' })
          }}
        >
          0.1%
        </div>
        <div
          className={`py-1 px-3 bg-[#1F2024] text-[#E3E4FF] font-medium text-sm ${
            eq(slippageTolerance, 0.005) ? 'ring-1 ring-inset ring-[#72D4B9]' : ''
          } cursor-pointer`}
          onClick={() => {
            useAppSettings.setState({ slippageTolerance: '0.005' })
          }}
        >
          0.5%
        </div>
        <div
          className={`py-1 px-3 bg-[#1F2024] text-[#E3E4FF] font-medium text-sm ${
            eq(slippageTolerance, 0.01) ? 'ring-1 ring-inset ring-[#72D4B9]' : ''
          } cursor-pointer`}
          onClick={() => {
            useAppSettings.setState({ slippageTolerance: '0.01' })
          }}
        >
          1%
        </div>
        <div
          className={`py-1 px-3 bg-[#1F2024] text-[#E3E4FF] font-medium text-sm ${
            !(eq(slippageTolerance, 0.001) || eq(slippageTolerance, 0.005) || eq(slippageTolerance, 0.01))
              ? 'ring-1 ring-inset ring-[#72D4B9]'
              : ''
          }`}
        >
          <Row>
            <Input
              className="w-[32px]"
              value={toString(mul(slippageTolerance, 100), { decimalLength: 'auto 2' })}
              onUserInput={(value) => {
                const n = div(parseFloat(value || '0'), 100)
                if (n) {
                  useAppSettings.setState({ slippageTolerance: n })
                }
              }}
              pattern={/^\d*\.?\d*$/}
            />
            <div>%</div>
          </Row>
        </div>
      </Row>
      {(slippageToleranceState === 'invalid' || slippageToleranceState === 'too small') && (
        <div
          className={`mt-4 mobile:mt-6 ${
            slippageToleranceState === 'invalid' ? 'text-[#D47B9A]' : 'text-[#D4B798]'
          } text-xs mobile:text-sm`}
        >
          {slippageToleranceState === 'invalid'
            ? 'Please enter a valid slippage percentage'
            : 'Your transaction may fail'}
        </div>
      )}
    </div>
  )
}

function CommunityPanelSidebarWidget() {
  return (
    <PageLayoutPopoverDrawer renderPopoverContent={<CommunityPopover />}>
      <OptionItem iconSrc="/icons/msic-community.svg">Community</OptionItem>
    </PageLayoutPopoverDrawer>
  )
}

function CommunityPopover() {
  function Item({
    text,
    iconSrc,
    href,
    onClick
  }: {
    text: string
    iconSrc: string
    href?: LinkAddress
    onClick?: (payload: { text: string; iconSrc: string; href?: LinkAddress }) => void
  }) {
    return (
      <Row
        className="gap-3 py-4 pl-4 pr-12 cursor-pointer"
        onClick={() => {
          if (href) linkTo(href)
          onClick?.({ text, iconSrc, href })
        }}
      >
        <Icon iconSrc={iconSrc} />
        <Link href={href} className="co-primary">
          {text}
        </Link>
      </Row>
    )
  }

  return (
    <>
      <div className="pt-3 -mb-1 mobile:mb-2 px-6 text-[rgba(164,170,245,0.5)] text-xs mobile:text-sm">COMMUNITY</div>
      <div className="gap-3 divide-y-1.5 divide-[rgba(164,170,245,0.2)] ">
        <Item href="" iconSrc="/icons/media-twitter.svg" text="Twitter" />
        <Item href="" iconSrc="/icons/media-discord.svg" text="Discord" />
        <PageLayoutPopoverDrawer
          renderPopoverContent={({ close }) => (
            <Col className="divide-y-1.5 divide-[rgba(164,170,245,0.2)]">
              <Item
                href=""
                iconSrc="/icons/media-telegram.svg"
                text="Telegram (EN)"
                onClick={close}
              />
              <Item
                href=""
                iconSrc="/icons/media-telegram.svg"
                text="Telegram (CN)"
                onClick={close}
              />
              <Item
                href=""
                iconSrc="/icons/media-telegram.svg"
                text="Telegram (KR)"
                onClick={close}
              />
              <Item
                href=""
                iconSrc="/icons/media-telegram.svg"
                text="Telegram (JP)"
                onClick={close}
              />
              <Item
                href=""
                iconSrc="/icons/media-telegram.svg"
                text="Telegram (ES)"
                onClick={close}
              />
              <Item
                href=""
                iconSrc="/icons/media-telegram.svg"
                text="Telegram (TR)"
                onClick={close}
              />
              <Item
                href=""
                iconSrc="/icons/media-telegram.svg"
                text="Telegram (VN)"
                onClick={close}
              />
              <Item
                href=""
                iconSrc="/icons/media-telegram.svg"
                text="Telegram (RU)"
                onClick={close}
              />
              <Item
                href=""
                iconSrc="/icons/media-telegram.svg"
                text="Telegram (TH)"
                onClick={close}
              />
            </Col>
          )}
        >
          <Row className="flex items-center justify-between">
            <Item iconSrc="/icons/media-telegram.svg" text="Telegram" />
            <Icon
              heroIconName="chevron-right"
              size="sm"
              className="justify-self-end m-2 text-[rgba(164,170,245,0.5)]"
            />
          </Row>
        </PageLayoutPopoverDrawer>

        <Item href="" iconSrc="/icons/media-medium.svg" text="Medium" />
      </div>
    </>
  )
}

function RpcConnectionPanelSidebarWidget() {
  return (
    <PageLayoutPopoverDrawer renderPopoverContent={({ close }) => <RpcConnectionPanelPopover close={close} />}>
      <RpcConnectionFace />
    </PageLayoutPopoverDrawer>
  )
}

function RpcConnectionFace() {
  const currentEndPoint = useConnection((s) => s.currentEndPoint)
  const isLoading = useConnection((s) => s.isLoading)
  const loadingCustomizedEndPoint = useConnection((s) => s.loadingCustomizedEndPoint)
  const extractConnectionName = useConnection((s) => s.extractConnectionName)
  const isMobile = useAppSettings((s) => s.isMobile)

  return (
    <div className="block py-4 mobile:py-3 px-8 pl-6 mobile:px-5 hover:bg-[rgba(57,208,216,0.1)] active:bg-[rgba(41,157,163,0.3)] cursor-pointer group">
      <Row className="items-center w-full mobile:justify-center">
        <div className="h-4 w-4 mobile:w-3 mobile:h-3 grid place-items-center mr-3 ">
          {isLoading ? (
            <Icon iconClassName="h-4 w-4 mobile:w-3 mobile:h-3" iconSrc="/icons/loading-dual.svg" />
          ) : (
            <div
              className={`w-1.5 h-1.5 mobile:w-1 mobile:h-1 bg-[#2de680] text-[#2de680]`}
              style={{
                boxShadow: '0 0 6px 1px currentColor'
              }}
            />
          )}
        </div>
        <span
          className="text-[rgba(172,227,229)] text-sm mobile:text-xs font-medium flex-grow overflow-ellipsis overflow-hidden"
          title={currentEndPoint?.url}
        >
          {currentEndPoint
            ? isLoading
              ? `RPC (${
                  (loadingCustomizedEndPoint?.name ?? extractConnectionName(loadingCustomizedEndPoint?.url ?? '')) || ''
                })`
              : `RPC (${(currentEndPoint?.name ?? extractConnectionName(currentEndPoint.url)) || ''})`
            : '--'}
        </span>
        <Icon size={isMobile ? 'xs' : 'sm'} heroIconName="chevron-right" iconClassName="text-[#ACE3E6]" />
      </Row>
    </div>
  )
}
function RpcConnectionPanelPopover({ close: closePanel }: { close: () => void }) {
  const availableEndPoints = useConnection((s) => s.availableEndPoints)
  const currentEndPoint = useConnection((s) => s.currentEndPoint)
  const autoChoosedEndPoint = useConnection((s) => s.autoChoosedEndPoint)
  const userCostomizedUrlText = useConnection((s) => s.userCostomizedUrlText)
  const switchConnectionFailed = useConnection((s) => s.switchConnectionFailed)
  const switchRpc = useConnection((s) => s.switchRpc)
  const deleteRpc = useConnection((s) => s.deleteRpc)
  const isLoading = useConnection((s) => s.isLoading)
  const isMobile = useAppSettings((s) => s.isMobile)

  return (
    <>
      <div className="pt-3 -mb-1 mobile:mb-2 px-6 mobile:px-3 text-[rgba(164,170,245,0.5)] text-xs mobile:text-sm">
        RPC CONNECTION
      </div>
      <div className="gap-3 divide-y-1.5">
        {availableEndPoints.map((endPoint) => {
          const isCurrentEndPoint = currentEndPoint?.url === endPoint.url
          return (
            <Row
              key={endPoint.url}
              className="group flex-wrap gap-3 py-4 px-6 mobile:px-3 border-[rgba(164,170,245,0.05)]"
              onClick={() => {
                if (endPoint.url !== currentEndPoint?.url) {
                  switchRpc(endPoint).then((result) => {
                    if (result === true) {
                      closePanel()
                    }
                  })
                }
              }}
            >
              <Row className="items-center w-full">
                <Row
                  className={`${
                    isCurrentEndPoint
                      ? 'text-[rgba(255,255,255,0.85)]'
                      : 'hover:co-primary active:co-primary co-primary cursor-pointer'
                  } items-center w-full`}
                >
                  {endPoint.name ?? '--'}
                  {endPoint.url === autoChoosedEndPoint?.url && <Badge className="self-center ml-2">recommended</Badge>}
                  {endPoint.isUserCustomized && (
                    <Badge className="self-center ml-2" cssColor="#c4d6ff">
                      user added
                    </Badge>
                  )}
                  {isCurrentEndPoint && (
                    <Icon
                      className="justify-self-end ml-auto text-[rgba(255,255,255,0.85)] clickable  transition"
                      iconClassName="ml-6"
                      heroIconName="check"
                    ></Icon>
                  )}
                  {endPoint.isUserCustomized && !isCurrentEndPoint && (
                    <Icon
                      className="justify-self-end ml-auto text-red-600 clickable opacity-0 group-hover:opacity-100 transition"
                      iconClassName="ml-6"
                      heroIconName="trash"
                      onClick={({ ev }) => {
                        deleteRpc(endPoint.url)
                        ev.stopPropagation()
                      }}
                    ></Icon>
                  )}
                </Row>
                {isLoading && endPoint === currentEndPoint && (
                  <Icon className="ml-3" iconClassName="h-4 w-4" iconSrc="/icons/loading-dual.svg" />
                )}
              </Row>
            </Row>
          )
        })}

        <Row className="border-[rgba(164,170,245,0.05)] items-center gap-3 p-4 mobile:py-4 mobile:px-2">
          <Input
            value={userCostomizedUrlText}
            className={`px-2 py-2.5 border-1.5 flex-grow ${
              switchConnectionFailed
                ? 'border-[#D47B9A]'
                : userCostomizedUrlText === currentEndPoint?.url
                ? 'border-[rgba(196,214,255,0.8)]'
                : 'border-[rgba(196,214,255,0.2)]'
            } min-w-[7em]`}
            inputClassName="font-medium text-[rgba(196,214,255,0.5)] placeholder-[rgba(196,214,255,0.5)]"
            placeholder="https://"
            onUserInput={(searchText) => {
              useConnection.setState({ userCostomizedUrlText: searchText })
            }}
            onEnter={() => {
              switchRpc({ url: userCostomizedUrlText }).then((isSuccess) => {
                if (isSuccess === true) {
                  closePanel()
                }
              })
            }}
          />
          <Button
            className="frosted-glass-lightsmoke"
            onClick={() => {
              switchRpc({ url: userCostomizedUrlText }).then((isSuccess) => {
                if (isSuccess === true) {
                  closePanel()
                }
              })
            }}
          >
            Switch
          </Button>
        </Row>
      </div>
    </>
  )
}
