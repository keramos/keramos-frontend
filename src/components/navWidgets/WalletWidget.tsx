import React, { ReactNode, useEffect } from 'react'

import useAppSettings from '@/application/appSettings/useAppSettings'
import useWallet from '@/application/wallet/useWallet'
import copyToClipboard from '@/functions/dom/copyToClipboard'
import useToggle from '@/hooks/useToggle'

import Button from '../Button'
import Icon from '../Icon'
import PageLayoutPopoverDrawer from '../PageLayoutPopoverDrawer'
import Row from '../Row'
import { FadeIn } from '../FadeIn'

function WalletWidgetItem({
  text,
  suffix,
  prefix,
  onClick
}: {
  text: string
  suffix?: ReactNode
  prefix?: ReactNode
  onClick?(): void
}) {
  return (
    <Row
      className="gap-2 py-4 px-6 border-[rgba(37,38,43,0.2)] cursor-pointer clickable clickable-filter-effect items-center"
      onClick={onClick}
    >
      {prefix}
      <div className="co-secondary text-xs whitespace-nowrap">{text}</div>
      {suffix}
    </Row>
  )
}

/** this should be used in ./Navbar.tsx */
export default function WalletWidget() {
  const isMobile = useAppSettings((s) => s.isMobile)
  const [isCopied, { delayOff, on }] = useToggle()

  useEffect(() => {
    if (isCopied) delayOff()
  }, [isCopied])

  const { owner: publicKey, disconnect, connected } = useWallet()

  return (
    <PageLayoutPopoverDrawer
      canOpen={connected}
      alwaysPopper
      popupPlacement="bottom-right"
      renderPopoverContent={({ close: closePanel }) => (
        <>
          <div className="pt-3 -mb-1 mobile:mb-2 px-6 text-[#666C80] text-xs mobile:text-xs">
            CONNECTED WALLET
          </div>
          <div className="gap-2 divide-y-1.5">
            <FadeIn noOpenTransitation>
              {publicKey && (
                <WalletWidgetItem
                  text={isCopied ? 'copied' : `${String(publicKey).slice(0, 7)}...${String(publicKey).slice(-7)}`}
                  suffix={
                    !isCopied && <Icon size="sm" className="clickable text-[#A4AAF5]" heroIconName="clipboard-copy" />
                  }
                  onClick={() => {
                    if (!isCopied) copyToClipboard(String(publicKey)).then(on)
                  }}
                />
              )}
            </FadeIn>
            <WalletWidgetItem
              prefix={<Icon size="sm" iconSrc="/icons/misc-recent-transactions.svg" />}
              text="RECENT TRANSACTIONS"
              onClick={() => {
                useAppSettings.setState({ isRecentTransactionDialogShown: true })
                closePanel?.()
              }}
            />
            <WalletWidgetItem
              prefix={<Icon size="sm" iconSrc="/icons/misc-disconnect-wallet.svg" />}
              text="DISCONNECT WALLET"
              onClick={() => {
                disconnect()
                closePanel?.()
              }}
            />
          </div>
        </>
      )}
    >
      {isMobile ? (
        <Button
          className="frosted-glass frosted-glass-lightsmoke p-1"
          onClick={() => {
            if (!publicKey) useAppSettings.setState({ isWalletSelectorShown: true })
          }}
        >
          <Icon
            className="w-5 h-5"
            iconClassName="w-5 h-5"
            iconSrc={connected ? '/icons/msic-wallet-connected.svg' : '/icons/msic-wallet.svg'}
          />
        </Button>
      ) : (
        <Button
          className="frosted-glass frosted-glass-lightsmoke text-xs px-4 py-1 font-light"
          onClick={() => {
            if (!publicKey) useAppSettings.setState({ isWalletSelectorShown: true })
          }}
        >
          {connected ? (
            <Row className="items-center gap-2 my-0">
              <Icon size="md" className="h-4 w-4" iconSrc="/icons/msic-wallet-connected.svg" />
              <div className="text-xs font-light co-primary">
                {String(publicKey).slice(0, 5)}...{String(publicKey).slice(-5)}
              </div>
            </Row>
          ) : (
            <Row className="items-center gap-2 my-0">
              <Icon size="md" className="h-4 w-4" iconSrc="/icons/msic-wallet.svg" />
              <div className="text-xs font-light co-primary">CONNECT WALLET</div>
            </Row>
          )}
        </Button>
      )}
    </PageLayoutPopoverDrawer>
  )
}
