import React from 'react'
import ReactMarkdown from 'react-markdown'

import { MessageBoardItem } from '@/application/messageBoard/type'
import useMessageBoard from '@/application/messageBoard/useMessageBoard'
import { isExist } from '@/functions/judgers/nil'

import Button from '../Button'
import Card from '../Card'
import Col from '../Col'
import Icon from '../Icon'
import PageLayoutPopoverDrawer from '../PageLayoutPopoverDrawer'
import ResponsiveDialogDrawer from '../ResponsiveDialogDrawer'
import Row from '../Row'
import linkTo from '@/functions/dom/linkTo'
import Link from '../Link'
import useAppSettings from '@/application/appSettings/useAppSettings'

/**
 * pure appearance component
 */
function MessageItem({
  messageBoardItem: item,
  haveReaded,
  onClick
}: {
  messageBoardItem: MessageBoardItem
  haveReaded?: boolean
  onClick?: () => void
}) {
  return (
    <Col
      className="py-4 px-2 border-[rgba(164,170,245,0.2)] cursor-pointer clickable clickable-filter-effect"
      onClick={onClick}
    >
      <Row className="gap-4 items-center">
        <div className={`text-[#E3E4FF] ${haveReaded ? 'opacity-40' : 'opacity-100'} font-medium text-sm`}>{item.title}</div>
        <Icon
          size="sm"
          className={`text-[#A4AAF5] ${haveReaded ? 'opacity-40' : 'hidden'}`}
          heroIconName="check-circle"
        />
      </Row>
      <div className={`text-[#80859C] ${haveReaded ? 'opacity-40' : 'opacity-100'} text-xs`} style={{marginTop:'4px'}}>
        {item.summary}
      </div>
    </Col>
  )
}

/** this should be used in ./Navbar.tsx */
export default function MessageBoardWidget() {
  const readedIds = useMessageBoard((s) => s.readedIds)
  const messageBoardItems = useMessageBoard((s) => s.messageBoardItems)
  const currentMessageBoardItem = useMessageBoard((s) => s.currentMessageBoardItem)
  const isMobile = useAppSettings((s) => s.isMobile)

  return (
    <>
      <PageLayoutPopoverDrawer
        alwaysPopper
        popupPlacement="bottom-right"
        renderPopoverContent={({ close: closePanel }) => (
          <div>
            <div className="pt-3 -mb-1 mobile:mb-2 px-6 text-[rgba(164,170,245,0.5)] text-xs mobile:text-sm">
              Update
            </div>
            <div className="gap-3 divide-y-1.5 p-4">
              {messageBoardItems.map((item) => (
                <MessageItem
                  key={item.title + item.updatedAt}
                  haveReaded={readedIds.has(item.id)}
                  messageBoardItem={item}
                  onClick={() => {
                    closePanel()
                    useMessageBoard.setState({ currentMessageBoardItem: item })
                  }}
                />
              ))}
            </div>
          </div>
        )}
      >
        <Icon
            className="w-5 h-5"
            iconClassName="w-5 h-5"
            iconSrc={'/icons/msic-noti.svg'}
          />
      </PageLayoutPopoverDrawer>
      <ResponsiveDialogDrawer
        open={isExist(currentMessageBoardItem)}
        onCloseTransitionEnd={() => {
          if (currentMessageBoardItem?.id) {
            useMessageBoard.setState((s) => ({
              readedIds: new Set(s.readedIds.add(currentMessageBoardItem.id))
            }))
          }
          useMessageBoard.setState({ currentMessageBoardItem: null })
        }}
      >
        {({ close }) => (
          <Card
            className="flex flex-col shadow-xl backdrop-filter backdrop-blur-xl p-8 mobile:p-8 w-[min(750px,100vw)] mobile:w-screen max-h-[min(850px,100vh)] mobile:h-screen border-1.5 border-[rgba(164,170,245,0.2)]"
            size="lg"
            style={{
              background:
                '#25262B',
              boxShadow: '0px 8px 48px rgba(171, 196, 255, 0.12)'
            }}
          >
            <Row className="justify-between items-center mb-6">
              <div className="text-sm mobile:text-xs font-light co-secondary">{currentMessageBoardItem?.title}</div>
              <Icon className="text-[#A4AAF5] cursor-pointer" heroIconName="x" onClick={close} />
            </Row>
            <div className="overflow-y-auto my-4">
              <ReactMarkdown
                className="my-6 whitespace-pre-line mobile:text-sm"
                components={{
                  p: (props) => <p className="text-sm text-[#80859C] mobile:text-xs" {...props} />,
                  li: ({ children }) => <li className="pl-2">{children}</li>,
                  ul: ({ children }) => <ul className="pl-6 list-disc">{children}</ul>,
                  h1: ({ children }) => <h1 className="co-primary text-lg font-light mobile:text-sm">{children}</h1>,
                  h2: ({ children }) => <h2 className="co-primary text-md font-light mobile:text-xs">{children}</h2>,
                  h3: ({ children }) => <h3 className="co-primary text-sm font-light mobile:text-2xs">{children}</h3>,
                  a: ({ children, href }) => <Link href={href}>{children}</Link>,
                  strong: ({ children }) => <span className="font-bold ">{children}</span>,
                  em: ({ children }) => <span className="italic text-xs mobile:text-xs">{children}</span>
                }}
              >
                {currentMessageBoardItem?.details ?? ''}
              </ReactMarkdown>
            </div>

            <Button className="frosted-glass-lightsmoke text-sm co-primary" onClick={close}>
              MARK AS READ
            </Button>
          </Card>
        )}
      </ResponsiveDialogDrawer>
    </>
  )
}
