import { useMemo } from 'react'

import Card, { CardProps } from './Card'

const paddingSize = 1

/**
 * only used in pools page and farm page
 */
export default function CyberpunkStyleCard({
  haveMinHeight,
  wrapperClassName,
  children,
  domRef,
  ...restProps
}: CardProps & { haveMinHeight?: boolean; wrapperClassName?: string }) {
  const borderRoundSize = useMemo(() => {
    if (restProps.style?.borderRadius) return `calc(${restProps.style.borderRadius} + ${paddingSize}px)`
    if (restProps.className?.includes('round-2xl')) return 16 + paddingSize
    if (restProps.size === 'lg') return 20 + paddingSize
    if (restProps.size === 'md') return 6 + paddingSize
  }, [restProps.className, restProps.size, restProps.style?.borderRadius])
  return (
    <div
      ref={domRef as any}
      className={wrapperClassName}
      style={{
        minHeight: haveMinHeight ? '300px' : undefined, // or style will be freak
        borderRadius: 0,
        padding: paddingSize,
        backgroundImage: 'linear-gradient(90deg, #d3eaed 0%, #e2cdf7 30%, #86a2db 100%)',
        boxShadow: '0 3px 6px -4px rgb(206 229 232 / 48%), 0 6px 16px 0 rgb(221 200 242 / 32%), 0 9px 28px 8px rgb(164 170 245 / 20%)'
      }}
    >
      <Card {...restProps}>{children}</Card>
    </div>
  )
}
