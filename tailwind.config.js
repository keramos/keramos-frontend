const { ibasic, imix } = require('./src/styles/basicUtilsPlugins')
const { cyberpunkLightBorders, cyberpunkBgLight, glassStyle } = require('./src/styles/themeStylePlugins')

/**@type {import("tailwindcss/tailwind-config").TailwindConfig} */
const config = {
  mode: 'jit',
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        // pc first!!
        pc: { max: '99999px' },
        tablet: { max: '1280px' },
        tabletv: { max: '1024px' },
        mobile: { max: '820px' }
      },
      colors: {
        // 👇 app color
        primary: 'var(--primary)', // text color
        secondary: 'var(--secondary)', // text color
        'style-color-fuchsia': '#D47B9A',
        'style-color-blue': '#2b6aff',
        'style-color-cyan': '#72D4B9',

        // 👇 state color
        'status-active': 'var(--status-active)',

        // 👇 text color
        'text-home-page-primary': 'var(--text-home-page-primary)',
        'text-home-page-secondary': 'var(--text-home-page-secondary)',
        'big-title': '#E3E4FF',
        'secondary-title': '#A4AAF5',

        // 👇 ground color (like: <Card>'s background)
        'ground-color-light-solid': 'var(--ground-color-light-solid)',
        'ground-color-light': 'var(--ground-color-light)',
        'ground-color-slim': 'var(--ground-color-slim)',
        'ground-color': 'var(--ground-color)',
        'ground-color-dark': 'var(--ground-color-dark)',
        'ground-color-dark-solid': 'var(--ground-color-dark-solid)',

        // 👇 formkit color (link: <Button> or <Switch>)
        'formkit-label-text-active': 'var(--formkit-label-text-active)',
        'formkit-label-text-normal': 'var(--formkit-label-text-normal)',
        // heavier than label text
        'formkit-thumb-text-normal': 'var(--formkit-thumb-text-normal)',
        'formkit-thumb-text-disabled': 'var(--formkit-thumb-text-disabled)',
        'formkit-bg-active': 'var(--formkit-bg-active)',
        'formkit-bg-normal': 'var(--formkit-bg-normal)',
        // heavier than formkit bg
        'formkit-thumb': 'var(--formkit-thumb-color)',
        'formkit-thumb-disable': 'var(--formkit-thumb-color-disable)',
        'formkit-thumb-transparent': 'var(--formkit-thumb-color-transparent)',

        // 👇 component color
        'pop-over-bg-high': 'var(--pop-over-bg-high)',
        'pop-over-bg-low': 'var(--pop-over-bg-low)',
        'pop-over-ring': 'var(--pop-over-ring)',
        'pop-over-ring-2': 'var(--pop-over-ring-2)',
        'link-color': 'var(--link-color)',
        'link-decorator': 'var(--link-decorator)',
        'card-color': 'var(--card-color)',
        'app-line': 'var(--app-line-color)',
        'formkit-ground': 'var(--formkit-ground-color)',
        'formkit-line': 'var(--formkit-line-color)',

        // 👇🎃 other color
        'dropzone-card-bg': 'var(--card-color-dark)'
      },
      fontSize: {
        '2xs': '10px'
      },
      spacing: {
        0.125: '0.5px',
        0.25: '1px',
        0.375: '1.5px',
        0.5: '2px',
        scrollbar: '7px' // defined in initialize.css
      },
      /** @see https://semi.design/en-US/basic/tokens#z-index */
      zIndex: {
        backtop: '10',
        'model-mask': '999',
        'drawer-mask': '999',
        model: '1000',
        drawer: '1000',
        notification: '1010',
        popover: '1030',
        dropdown: '1050',
        tooltip: '1060'
      },
      borderWidth: {
        1.5: '1.5px'
      },
      borderRadius: {
        '3xl': '0'
      },
      ringWidth: {
        1.5: '1.5px'
      },
      flexGrow: {
        2: '2'
      },
      gridTemplateColumns: {
        '1-fr': '1fr',
        '2-fr': '1fr 1fr',
        '3-fr': '1fr 1fr 1fr',

        '4-fr': '1fr 1fr 1fr 1fr',
        '5-fr': '1fr 1fr 1fr 1fr 1fr',
        '6-fr': '1fr 1fr 1fr 1fr 1fr 1fr',
        '1-auto': 'auto',
        '2-auto': 'auto auto',
        '3-auto': 'auto auto auto',
        '4-auto': 'auto auto auto auto',
        '5-auto': 'auto auto auto auto auto',
        '6-auto': 'auto auto auto auto auto auto',
        'auto-fit': 'repeat(auto-fit, minmax(0, 1fr))'
      },
      transitionDuration: {
        3000: '3000ms', // for debug
        8000: '8000ms', // for debug
        'extremely-slow': '100s' // for debug
      },
      backgroundImage: {
        'popup-bg':
          '#25262B'
      },
      dropShadow: {
        'popup-white': '0px 8px 48px rgba(164, 170, 245, 0.3))' // drop-shadow(0px 0px 2px rgba(171, 196, 255, 0.5))'
      }
    }
  },
  variants: {
    extend: {
      backgroundColor: ['even', 'odd'], // need it if JIT ?
      brightness: ['hover', 'focus', 'active'], // need it if JIT ?
      scale: ['active'] // need it if JIT ?
    }
  },
  plugins: [ibasic, imix, cyberpunkLightBorders, cyberpunkBgLight, glassStyle]
}

module.exports = config
