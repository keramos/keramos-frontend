
# 🚀 Swap, Farm, Liquidity Pool is live


New features added for **KERAMOS V.0.3**

For product demonstration, FAQs you may read here [KERAMOS Docs](https://keramos.gitbook.io/keramos-docs/)

*Please note that this is a beta version of KERAMOS, which is still undergoing final testing before official release. The platform, its software and all content found on it are provided on an “as is” and “as available” basis. KERAMOS does not give any warranties, whether express or implied, as to the suitability or usability of the website.*
